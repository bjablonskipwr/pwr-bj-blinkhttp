﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raspberry.IO.GeneralPurpose;
using System.Net;


namespace BlinkHttp
{
    class Program
    {

        static int currentNumber = 0;
        static GpioConnection connection;
        static PinConfiguration[] pins = new PinConfiguration[6];

        static string rootUrl = "http://192.168.1.25:8080/test/";

        static void Main(string[] args)
        {
            char localKey = ' ';
            
            if ((args != null) && (args.Length > 0))
            {
                rootUrl = args[0];
            }

            #region Pins configuration
            pins[0] = ConnectorPin.P1Pin11.Output();
            pins[1] = ConnectorPin.P1Pin13.Output();
            pins[2] = ConnectorPin.P1Pin15.Output();

            pins[3] = ConnectorPin.P1Pin22.Output();
            pins[4] = ConnectorPin.P1Pin24.Output();
            pins[5] = ConnectorPin.P1Pin26.Output();

            var switchButton = ConnectorPin.P1Pin03.Input()
                .Name("Switch")
                .Revert()
                .Switch()
                .Enable()
                .OnStatusChanged(b =>
                {
                    Console.WriteLine("Button switched {0}", b ? "on" : "off");
                    connection.Pins[pins[0]].Enabled = b;
                });
            #endregion

            #region Connection and webserver init
            Console.WriteLine("Connection Init Start");
            connection = new GpioConnection(pins);
            connection.Add(switchButton);
            Console.WriteLine("Connection Init Complete");
            Console.WriteLine();

            Console.WriteLine("Webserver Init Start");
            WebServer ws = new WebServer(SendResponse, rootUrl);
            ws.Run();
            Console.WriteLine("Webserver Init Complete at " + rootUrl);
            Console.WriteLine();
            #endregion

            #region Main loop
            while (localKey != 'q')
            {
                localKey = Console.ReadKey().KeyChar;
                
                Task t = Task.Run(() =>
                {
                    Console.WriteLine("Local Blink at " + currentNumber.ToString());
                    connection.Blink(pins[currentNumber], 250);
                });

            }
            #endregion 

            connection.Close();
            ws.Stop();
        }

        public static string SendResponse(HttpListenerRequest request)
        {
            currentNumber = ++currentNumber % 6;
            
            Task t = Task.Run(() =>
            {
                Console.WriteLine("Http Blink at " + currentNumber.ToString());
                connection.Blink(pins[currentNumber], 250);
            });

            return string.Format("<HTML><BODY>My web page.<br> Current number: {0} at {1}<br/> <a href=\"{2}\">Blink!</a></BODY></HTML>", currentNumber, DateTime.Now, rootUrl);
        }

    }
}

    